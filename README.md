Install:

mkdir /var/www/html/admin/modules/sipcomment
cp * /var/www/html/admin/modules/sipcomment/

Go into FreePBX and under modules, enable SIP Comment

Usage:

Under Extensions you will now have an extra field. Anything
added to this will be prepended as a commented out line in
sip_additional.conf

Bugs/Issues/Suggestions to pieter@insync.za.net

Current repo: https://gitlab.com/dewitpj/sipcomment
